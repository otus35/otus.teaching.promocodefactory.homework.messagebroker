﻿using CommonNamespace;
using MassTransit;
using Microsoft.Extensions.Logging;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Abstractions.Services;
using System.Threading.Tasks;

namespace Otus.Teaching.Pcf.GivingToCustomer.Core.Consumers
{
    public class GivePromoCodeToCustomerConsumer : IConsumer<GivePromoCodeToCustomerDto>
    {
        private readonly IPromoCodeService _promoCodeService;
        readonly ILogger<GivePromoCodeToCustomerConsumer> _logger;
        
        public GivePromoCodeToCustomerConsumer(IPromoCodeService promoCodeService,
                                               ILogger<GivePromoCodeToCustomerConsumer> logger)
        {
            _promoCodeService = promoCodeService;
            _logger = logger;
        }
        public async Task Consume(ConsumeContext<GivePromoCodeToCustomerDto> context)
        {
            var request = context.Message;
            _logger.LogInformation($"Received PromoCode:{request.PromoCode}");

            await _promoCodeService.SavePromoCode(request);
        }
    }
}
