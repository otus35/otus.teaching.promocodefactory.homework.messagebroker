﻿using CommonNamespace;
using MassTransit;
using Microsoft.Extensions.Logging;
using Otus.Teaching.Pcf.Administration.Core.Abstractions.Services;
using System.Threading.Tasks;

namespace Otus.Teaching.Pcf.Administration.Core.Consumers
{
    public class GivePromoCodeToCustomerConsumer : IConsumer<GivePromoCodeToCustomerDto>
    {
        private readonly IEmployeeService _employeeService;
        readonly ILogger<GivePromoCodeToCustomerConsumer> _logger;
        
        public GivePromoCodeToCustomerConsumer(IEmployeeService employeeService,
                                               ILogger<GivePromoCodeToCustomerConsumer> logger)
        {
            _employeeService = employeeService;
            _logger = logger;
        }
        public async Task Consume(ConsumeContext<GivePromoCodeToCustomerDto> context)
        {
            var request = context.Message;
            _logger.LogInformation($"Received PromoCode:{request.PromoCode}");

            if (request.PartnerManagerId.HasValue)
            {
                await _employeeService.UpdateAppliedPromocodesAsync(request.PartnerManagerId.Value);
            }
        }
    }
}
