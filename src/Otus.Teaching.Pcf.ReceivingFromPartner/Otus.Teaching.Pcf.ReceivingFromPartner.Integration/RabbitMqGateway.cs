﻿using System;
using System.Threading;
using System.Threading.Tasks;
using CommonNamespace;
using MassTransit;
using Microsoft.Extensions.Configuration;
using Otus.Teaching.Pcf.ReceivingFromPartner.Core.Abstractions.Gateways;
using Otus.Teaching.Pcf.ReceivingFromPartner.Core.Domain;

namespace Otus.Teaching.Pcf.ReceivingFromPartner.Integration
{
    public class RabbitMqGateway : IRabbitMqGateway
    {
        private readonly IBusControl _busControl;
        public IConfiguration Configuration { get; }

        public RabbitMqGateway(IBusControl busControl, 
                               IConfiguration configuration)
        {
            Configuration = configuration;
            _busControl = busControl;
        }

        public async Task GivePromoCodeToCustomer(PromoCode promoCode)
        {
            await _busControl.Publish(new GivePromoCodeToCustomerDto()
            {
                PartnerId = promoCode.Partner.Id,
                BeginDate = promoCode.BeginDate.ToShortDateString(),
                EndDate = promoCode.EndDate.ToShortDateString(),
                PreferenceId = promoCode.PreferenceId,
                PromoCode = promoCode.Code,
                PromoCodeId = promoCode.Id,
                ServiceInfo = promoCode.ServiceInfo,
                PartnerManagerId = promoCode.PartnerManagerId
            }, CancellationToken.None);

            /*
            string queueName = Configuration["RabbitMqConnection:QueueName"];            
                        
            var sendEndpoint = await _busControl.GetSendEndpoint(new Uri($"queue:{queueName}"));
            if (sendEndpoint == null)
            {
                throw new Exception($"Не удалось найти очередь {queueName}");
            }
            await sendEndpoint.Send(new GivePromoCodeToCustomerDto()
            {
                PartnerId = promoCode.Partner.Id,
                BeginDate = promoCode.BeginDate.ToShortDateString(),
                EndDate = promoCode.EndDate.ToShortDateString(),
                PreferenceId = promoCode.PreferenceId,
                PromoCode = promoCode.Code,
                PromoCodeId = promoCode.Id,
                ServiceInfo = promoCode.ServiceInfo,
                PartnerManagerId = promoCode.PartnerManagerId
            }, CancellationToken.None);*/
        }
    }
}